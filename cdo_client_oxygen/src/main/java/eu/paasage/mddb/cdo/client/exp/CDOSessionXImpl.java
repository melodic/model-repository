package eu.paasage.mddb.cdo.client.exp;

import eu.paasage.mddb.cdo.client.OCLValidation;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.session.CDOSession;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

public class CDOSessionXImpl implements CDOSessionX {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(CDOSessionXImpl.class);

    private CDOSession session;
    private boolean logging;

    public CDOSessionXImpl(CDOSession session, boolean logging) {
        this.session = session;
        this.logging = logging;
    }

    /* This method can be used to open a CDO transaction and return it to
     * the developer/user. The developer/user should not forget to close
     * the respective cdo transaction in the end.
     */
    @Override
    public CDOTransaction openTransaction(){
        CDOTransaction trans = session.openTransaction();
        return trans;
    }

    @Override
    public void closeTransaction(CDOTransaction tr) {
        if (tr != null) {
            if (logging) log.info("Transaction is not null");
            if (tr.isClosed()) {
                if (logging) log.info("Closing transaction");
                tr.close();
                if (logging) log.info("Transaction has been closed!");
            } else {
                if (logging) log.info("Transaction is already closed");
            }
        } else {
            if (logging) log.info("Transaction is null");
        }
    }

    /* This method can be used to open a CDO view and return it to
     * the developer/user. The developer/user should not forget to close
     * the respective cdo view in the end.
     */
    @Override
    public CDOView openView(){
        CDOView view = session.openView();
        if (logging) log.info("Opened view!");
        return view;
    }

    @Override
    public void storeModels(EObject model, String resourceName) {
        storeModels(model, resourceName, false);
    }

    @Override
    public void storeModels(EObject model, String resourceName, boolean validate) {
        CDOTransaction trans = openTransaction();
        CDOResource cdo = trans.getOrCreateResource(resourceName);
        EList<EObject> list = cdo.getContents();

        list.add(model);

        try {
            if (validate) {
                if (!OCLValidation.validate(model)){
                    trans.rollback();
                    trans.close();
                }
            }
            trans.commit();
            trans.close();
        } catch (Exception e) {
            log.error("Problem during saving model {} under path: {}", model, resourceName, e);
        }
    }

    @Override
    public void closeSession(){
        if (session != null && !session.isClosed()){
            session.close();
        }
    }

    @Override
    public CDOSession getSession(){
        return session;
    }

}
