package eu.paasage.mddb.cdo.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CdoServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CdoServerApplication.class, args);
    }
}
